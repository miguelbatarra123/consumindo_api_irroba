
@include('layout.header')

<div>

        <div class="container col-md-12" style="margin-top: 30px; padding: 0;">
            <h1 style="font-weight: bolder">Informações do produto adicionado</h1>
            <h4>ID do produto: {{$response['product_id']}}</h4>
            <h4>Nome: {{$response['name']}}</h4>
            <h4>Preço: {{$response['price']}}</h4>
            <h4>SKU: {{$response['sku']}}</h4>
            <h4>ID da categoria: {{$response['product_category']}}</h4>
            
            <div class="pt-xl-5 pl-xl-4">
                <a href="{{ route('myProducts') }}/delete/{{$response['product_id']}}">
                    <button style="margin-right: 20px" class="btn btn-danger">Remover da lista</button>
                </a>

                <a href="{{ route('myProducts') }}">
                    <button class="btn btn-primary">Voltar</button>
                </a>
            </div>

        </div>


</div>

<footer class="bg-dark text-center text-white" style="width: 100%; margin: auto; bottom: 0; position: fixed;">
    <div class="container p-4 pb-0">
        <section class="mb-4">
        <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-facebook-f"></i
        ></a>

        <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-twitter"></i
        ></a>

        <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-google"></i
        ></a>

        <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-instagram"></i
        ></a>

        <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-linkedin-in"></i
        ></a>

        <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-github"></i
        ></a>
        </section>
    </div>

    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 Copyright:
        <a class="text-white" href="#">Miguel Moscardini Battarra</a>
    </div>
    </footer>
</body>
</html>