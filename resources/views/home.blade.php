@include('layout.header')

    <div class="text-center" style="margin: 20px">
        <h1>Produtos</h1>
    </div>

    <div>
        <div>
            <div class="row" style="display: flex; justify-content: center;">

                @forelse($response as $res)

                    <div class="card col-md-3" style="margin: 20px;">
                        <img class="card-img-top" src="https://source.unsplash.com/300x200/?space/{{ $res['product_id'] }}" alt="Sem imagem">
                        <div class="card-body">
                            <h5 class="card-title">{{$res['product_description'][0]['name']}} - {{ $res['product_id'] }}</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto voluptatum numquam expedita quisquam fugit mollitia exercitationem itaque incidunt. Maiores ducimus, laboriosam facere architecto accusantium cupiditate iste enim omnis possimus consectetur.</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <a href="{{ route('productsId') }}?id={{ $res['product_id'] }}">
                                    <button class="btn btn-info">Mais informações</button>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('insert') }}?id={{$res['product_id']}}">
                                    <button class="btn btn-success">Adicionar a lista</button>
                                </a>
                            </li>
                        </ul>
                    </div>

                @empty

                    <div class="text-center">
                        <h2>Não possui produtos!</h2>
                    </div>  

                @endforelse

            </div>
        </div>
    </div>

@include('layout.footer')
