<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\InsertProductService;


class InsertProductController extends Controller
{
    public function __construct(InsertProductService $insertProductService)
    {
        $this->insertProductService = $insertProductService;
    }


    public function insert(Request $request){

        return $this->insertProductService->insert($request);

    }

}
