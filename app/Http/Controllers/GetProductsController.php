<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 

use App\Services\GetProductsService;


class GetProductsController extends Controller
{ 

    public function __construct(GetProductsService $getProductsService){
        $this->getProductsService = $getProductsService;
    }
    
    public function index(Request $request)
    {
        return $this->getProductsService->getProducts($request);
    }

    
    public function show(Request $request)
    {
        return $this->getProductsService->getProductId($request);
    }


}
