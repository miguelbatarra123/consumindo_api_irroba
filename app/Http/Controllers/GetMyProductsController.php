<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\GetMyProductsService;

class GetMyProductsController extends Controller
{
    public function __construct(GetMyProductsService $getMyProductsService)
    {
        $this->getMyProductsService = $getMyProductsService;
    }


    public function index(Request $request){

        return $this->getMyProductsService->getMyProducts($request);

    }


    public function show(Request $request, $id){

        return $this->getMyProductsService->getMyProductsId($request, $id);

    }


    public function destroy(Request $request, $id){

        return $this->getMyProductsService->removeMyProduct($request, $id);

    }
}
