<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\GetLoginService;

class GetLoginController extends Controller
{
    public function __construct(GetLoginService $getLoginService)
    {
        $this->getLoginService = $getLoginService;
    }


    public function index(Request $request)
    {

        return $this->getLoginService->getLogin($request);

    }


    public function logout(Request $request)
    {

        return $this->getLoginService->getLogout($request);

    }
}
