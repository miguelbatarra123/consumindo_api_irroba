<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;


class InsertProductService
{
    
    public function insert($request){

        $key = $request->session()->get('_tokenApi');

        $response = Http::withToken($key)->post(env('BASE_API') . '/apiExternal/' . $request->id); 

        if (isset($response->json()['Error'])){

            return redirect('/');

        }
        
        $data = $response->json()['data'][0];

        $this->insertProduct($key, $data);

        $this->insertCategory($key, $data);

        $this->insertCategoryProduct($key, $data);

        return redirect('/home');

    }


    public function insertProduct($key, $data){

        $insert = Http::withToken($key)->post(env('BASE_API') . '/apiInternal/product', [

            'product_id' => $data['product_id']??'',
            'name' => $data['product_description'][0]['name']??'',
            'price' => $data['price']??'',
            'sku' => $data['sku']??''

        ]); 

        return $insert;

    }


    public function insertCategory($key, $data){

        $count = count($data['product_to_category']);

        $i = 0;

        do {

            $insert = Http::withToken($key)->post(env('BASE_API') . '/apiInternal/category', [

                'category_id' => $data['product_to_category'][$i]['category_id']??'',
                'category_name' => $data['product_to_category'][$i]['name']??''

            ]); 

            $i++;
        } while ($i < $count);

        return $insert;

    }


    public function insertCategoryProduct($key, $data){

        $count = count($data['product_to_category']);
        
        $i = 0;

        do {

            $insert = Http::withToken($key)->post(env('BASE_API') . '/apiInternal/product_category', [

                'product_id' => $data['product_id']??'',
                'category_id' => $data['product_to_category'][$i]['category_id']??''

            ]); 

            $i++;
        } while($i < $count);

        return $insert;

    }

}
