<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;


class GetProductsService
{

    public function getProducts($request){

        $key = $request->session()->get('_tokenApi');

        $response = Http::withToken($key)->post(env('BASE_API') . '/apiExternal'); 

        if (isset($response->json()['Error'])){

            return redirect('/');

        }

        $data = $response->json()['data'];


        return view('home', ['response' => $data]);
 
    }


    public function getProductId($request){

        $key = $request->session()->get('_tokenApi');

        $response = Http::withToken($key)->post(env('BASE_API') . '/apiExternal/' . $request->id); 

        if (isset($response->json()['Error'])){

            return redirect('/');

        }
        
        $data = $response->json()['data'][0];

        if (!$data['product_to_category']){

            $data['product_to_category'] = 'Nao possui Categoria';

        } else {

            $count = count($data['product_to_category']);

            $i = 0;

            $dataProductConcat = '';

            do {
                
                $dataProduct = $data['product_to_category'][$i]['category_id'];

                $dataProductConcat .= $dataProduct . ' ,'; 

                $i ++;
            
            } while($i < $count);
                
            $dataProductConcat = substr($dataProductConcat, 0, -1);

            $data['product_to_category'] = $dataProductConcat;
            
        }

        return view('productInformation', ['response' => $data]);

    }


}
