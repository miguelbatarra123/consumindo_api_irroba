<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;


class GetMyProductsService
{

    public function getMyProducts($request){

        $key = $request->session()->get('_tokenApi');

        $response = Http::withToken($key)->get(env('BASE_API') . '/apiInternal/product'); 

        if (isset($response->json()['Error'])){

            return redirect('/');

        }

        $data = $response->json();

        return view('myProduct', ['response' => $data]);

    }


    public function getMyProductsId($request, $id){

        $key = $request->session()->get('_tokenApi');

        $response = Http::withToken($key)->get(env('BASE_API') . '/apiInternal/product/' . $id); 
        
        if (isset($response->json()['Error'])){

            return redirect('/');

        }

        $data = $response->json();

        if (!$data['product_category'] || !$data['product_category'][0]['category_id']){

            $data['product_category'] = "Não possui categoria";

        } else{

            $count = count($data['product_category']);

            $i = 0;

            $dataProductConcat = '';

            do {
                
                $dataProduct = $data['product_category'][$i]['category_id'];

                $dataProductConcat .= $dataProduct . ' ,'; 

                $i ++;
            
            } while($i < $count);
                
            $dataProductConcat = substr($dataProductConcat, 0, -1);

            $data['product_category'] = $dataProductConcat;
                
        }
            
        return view('myProductInformation', ['response' => $data]);

    }


    public function removeMyProduct($request, $id){

        $key = $request->session()->get('_tokenApi');

        Http::withToken($key)->delete(env('BASE_API') . '/apiInternal/product/' . $id); 
        
        return redirect()->route('myProducts');

    }

}
