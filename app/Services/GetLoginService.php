<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;


class GetLoginService
{
    
    public function getLogin($request){

        $response = Http::post(env('BASE_API') . '/login', [
            'name' => $request->name,
            'password' => $request->password
        ]); 
        
        $request->session()->forget('Login');

        
        if ($response->ok()){

            $key = $response->json()['access_token'];
            
            $request->session()->put('_tokenApi', $key);
            
            return redirect()->route('home');
            
        } else {

            $request->session()->put('Login', 'Invalid User ');

            return redirect('/');

        }


    }


    public function getLogout($request){

        $key = $request->session()->get('_tokenApi');

        Http::withToken($key)->post(env('BASE_API') . '/logout');

        return redirect('/');

    }


}
