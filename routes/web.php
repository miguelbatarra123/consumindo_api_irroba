<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request; 


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return "view('welcome')";
// });

Route::get('/', function(){
    return view('login');
});

Route::post('/login', 'GetLoginController@index')->name('login');
Route::get('/logout', 'GetLoginController@logout')->name('logout');


Route::prefix('home')->group( function(){

    Route::get('/', 'GetProductsController@index')->name('home');
    Route::get('/product', 'GetProductsController@show')->name('productsId');
    Route::get('/insert', 'InsertProductController@insert')->name('insert');
    Route::get('/my_products', 'GetMyProductsController@index')->name('myProducts');
    Route::get('/my_products/{id}', 'GetMyProductsController@show');
    Route::get('/my_products/delete/{id}', 'GetMyProductsController@destroy');

});